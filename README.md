# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://aljazferko@bitbucket.org/aljazferko/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/aljazferko/stroboskop/commits/0440073ed878bef416106dcfd05fbac407336606

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aljazferko/stroboskop/commits/be73b6ae80e56a12a830a180c1281edac5ff9da3

Naloga 6.3.2:
https://bitbucket.org/aljazferko/stroboskop/commits/1fc4671756a35ddd3e28a2946c69f038e878550e

Naloga 6.3.3:
https://bitbucket.org/aljazferko/stroboskop/commits/e73bfb3edad2249b422393c613f4b08bf6e627aa

Naloga 6.3.4:
https://bitbucket.org/aljazferko/stroboskop/commits/4515283f8eaa258d91c18467a66d7e32d145b312

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aljazferko/stroboskop/commits/f8a7cb249b363d34bb2c5be8135c834954c3e459

Naloga 6.4.2:
https://bitbucket.org/aljazferko/stroboskop/commits/bba7e79d8aa667f7036d5f6835df945affe9dec4

Naloga 6.4.3:
https://bitbucket.org/aljazferko/stroboskop/commits/ce80aa2fcef8052a40d169faff8902fa661e33d8

Naloga 6.4.4:
https://bitbucket.org/aljazferko/stroboskop/commits/31646f1d062a17f14246dfae70df6441d12c3bc1